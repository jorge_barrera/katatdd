from unittest import TestCase

from estadistica import estadistica


class estadisticaTest(TestCase):
    # Test Paso 1 Contar
    def test_contar_elementos(self):
        self.assertEqual(estadistica().contar_elementos(""), 0, "Contar Cadena Vacia")

    def test_contar_numero(self):
        self.assertEqual(estadistica().contar_elementos("1"), 1, "Contar Un Numero")

    def test_contar_dos_numeros(self):
        self.assertEqual(estadistica().contar_elementos("1,2"), 2, "Contar Dos Numeros")

    def test_contar_numeros(self):
        self.assertEqual(estadistica().contar_elementos("1,2,3,4"), 4, "Contar Varios Numeros")

    # Test Paso 2 Minimo
    def test_numero_menor_cadena_vacia(self):
        self.assertEqual(estadistica().numero_menor(""), [0, 0], "Menor Numero Cadena Vacia")

    def test_numero_menor_cadena_un_elemento(self):
        self.assertEqual(estadistica().numero_menor("2"), [1, 2], "Menor Numero Un Elemento")

    def test_numero_menor_cadena_dos_elementos(self):
        self.assertEqual(estadistica().numero_menor("1,3"), [2, 1], "Menor Numero Dos Elementos")

    def test_numero_menor_cadena_varios_elementos(self):
        self.assertEqual(estadistica().numero_menor("8,9,4"), [3, 4], "Menor Numero Varios Elementos")

    # Test Paso 3 Maximo
    def test_numero_mayor_cadena_vacia(self):
        self.assertEqual(estadistica().numero_mayor(""), [0, 0, 0], "Mayor Numero Cadena Vacia")

    def test_numero_mayor_cadena_un_elemento(self):
        self.assertEqual(estadistica().numero_mayor("3"), [1, 3, 3], "Mayor Numero Un Elemento")

    def test_numero_mayor_cadena_dos_elementos(self):
        self.assertEqual(estadistica().numero_mayor("1,3"), [2, 1, 3], "Mayor Numero Dos Elementos")

    def test_numero_mayor_cadena_varios_elementos(self):
        self.assertEqual(estadistica().numero_mayor("8,4,9"), [3, 4, 9], "Mayor Numero Varios Elementos")

    # Test Paso 4 Promedio
    def test_promedio_cadena_vacia(self):
        self.assertEqual(estadistica().estadisticas(""), [0, 0, 0, 0], "Promedio Cadena Vacia")

    def test_promedio_cadena_un_elemento(self):
        self.assertEqual(estadistica().estadisticas("4"), [1, 4, 4, 4], "Promedio Cadena Un Elemento")

    def test_promedio_cadena_dos_elementos(self):
        self.assertEqual(estadistica().estadisticas("1,3"), [2, 1, 3, 2], "Promedio Cadena Dos Elementos")

    def test_promedio_cadena_varios_elementos(self):
        self.assertEqual(estadistica().estadisticas("8,4,9"), [3, 4, 9, 7], "Promedio Cadena Varios Elementos")