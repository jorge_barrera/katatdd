class estadistica:
    def contar_elementos(self, cadena):
        if (cadena != ""):
            if "," in cadena:
                return len(cadena.split(","))
            else:
                return 1
        else:
            return 0

    def numero_menor(self, cadena):
        if (cadena != ""):
            return [self.contar_elementos(cadena), int(min(cadena.split(",")))]
        else:
            return [self.contar_elementos(cadena), 0]

    def numero_mayor(self, cadena):
        menor = self.numero_menor(cadena)
        if (cadena != ""):
            return [menor[0], menor[1], int(max(cadena.split(",")))]
        else:
            return [menor[0], menor[1], 0]

    def estadisticas(self, cadena):
        mayor = self.numero_mayor(cadena)
        if (cadena != ""):
            numeros = cadena.split(",")
            suma = 0
            for num in numeros:
                suma = suma + int(num)
            return [mayor[0], mayor[1], mayor[2], suma/mayor[0]]
        else:
            return [mayor[0], mayor[1], mayor[2], 0]
